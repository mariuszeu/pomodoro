const POMODORO_STATES = {
    START: {name: 'Get ready?', time: 0},
    STOP: {name: 'stop', time: 0},
    WORK: {name: 'work', time: 25},
    REST: {name: 'rest', time: 5},
    PAUSE: {name: 'pause', time: 0}
};

const COLOURS = ['colour-my-0','colour-my-1'];

// var data = {
//     appName: "Pomodoro",
//     appname: "Pomodoro",
//     state: {
//         name: POMODORO_STATES.START.name,
//         parent: "", //aktualny status nadrzędny (praca, przerwa)
//         ico: "play_arrow"
//     },
//     second: 0, //aktualna ilość sekund dla aktywnego trybu (praca, przerwa)
//     works: 0,
//     rests: 0
// };


// components  -----------------------------------------------------------------------------------------
// var NameAndActionsComponent = Vue.extend({
//     data: function() {
//         return {};
//     },
//     props: ['appname','state','second'],
//     template: `
//         <h2>
//             <span>{{ appname }}</span>
//             <button class="btn btn-primary" @click="changeStatus">
//                 <i class="material-icons">{{ state.ico }}</i>
//             </button>
//             <button class="btn btn-primary" @click="changeMode" :disabled="state.name === 'Get ready?'">
//                 <i class="material-icons">redo</i>
//             </button>
//             <button class="btn btn-primary" @click="stop" :disabled="state.name === 'Get ready?'">
//                 <i class="material-icons">stop</i>
//             </button>
//         </h2>
//     `,
//     methods: {
//         changeStatus: function() {
//             // console.log("aktualny status przed zmianą", this.state.name);
//             if (this.state.name === POMODORO_STATES.START.name) {
//                 this.state.parent = POMODORO_STATES.WORK.name;
//                 this.start();
//             } else if (this.state.name === POMODORO_STATES.WORK.name) {
//                 this.state.parent = POMODORO_STATES.WORK.name;
//                 this.pause();
//             } else if (this.state.name === POMODORO_STATES.REST.name) {
//                 this.state.parent = POMODORO_STATES.REST.name;
//                 this.pause();
//             } else if (this.state.name === POMODORO_STATES.PAUSE.name) {
//                 this.start();
//             }
//             // console.log("aktualny status po zmianie", this.state.name);
//         },
//
//         start: function() { //rozpoczęcie pracy lub wznowienie po zapauzowaniu
//             console.log("START");
//             if (this.state.parent === POMODORO_STATES.WORK.name) {
//                 this.state.name = POMODORO_STATES.WORK.name;
//             } else if (this.state.parent === POMODORO_STATES.REST.name) {
//                 this.state.name = POMODORO_STATES.REST.name;
//             }
//             this.state.ico = "pause";
//             this.countTime('start');
//         },
//
//         stop: function() { //zatrzymanie Pomodoro i zresetowanie do ustawień początkowych
//             console.log("STOP");
//             this.state.name = POMODORO_STATES.START.name;
//             this.state.parent = "";
//             this.state.ico = "play_arrow";
//             this.countTime('stop');
//         },
//
//         pause: function() {
//             console.log("PAUSE");
//             this.state.name = POMODORO_STATES.PAUSE.name;
//             this.state.ico = "play_arrow";
//             this.countTime('pause');
//         },
//
//         changeMode: function() { //zmiana z praca>przerwa i przerwa>praca
//             if (this.state.parent === POMODORO_STATES.WORK.name) {
//                 console.log("Go to REST");
//
//                 this.countTime('stop');
//                 this.state.name = POMODORO_STATES.REST.name;
//                 this.state.parent = POMODORO_STATES.REST.name;
//                 this.countTime('start');
//                 this.state.ico = "pause";
//             } else if (this.state.parent === POMODORO_STATES.REST.name) {
//                 console.log("Go to WORK");
//
//                 this.countTime('stop');
//                 this.state.name = POMODORO_STATES.WORK.name;
//                 this.state.parent = POMODORO_STATES.WORK.name;
//                 this.countTime('start');
//                 this.state.ico = "pause";
//             }
//         },
//
//         countTime: function(action) {
//
//             if (action === "start") {
//                 this._interval = setInterval(function(){
//                     this.second += 1;
//
//                     //zmiana trybu na kolejny, kiedy upłynie zadeklarowany czas aktualnego trybu
//                     if (this.second === this.timeForState*60) {
//                         this.changeMode();
//                     }
//
//                 }.bind(this), 1000);
//             } else if (action === "pause") {
//                 clearInterval(this._interval);
//             } else if (action === "stop") {
//                 clearInterval(this._interval);
//                 this.second = 0;
//             }
//
//         }
//
//     }
//
// });

// var StatusAndInfoComponent = Vue.extend({
//     data: function() {
//         return {};
//     },
//     props: ['state'],
//     template: `
//     <span id="stateName" class="label label-primary">{{ state.name }}</span>
//     `
// });

// var CounterComponent = Vue.extend({
//     data: function() {
//         return {
//
//         };
//     },
//     // props: ['state','second'],
//     template: `
//     <div>
//         <!--<div class="toast toast-primary">-->
//             <!--<div class="pomodoro-timer">-->
//                 <!--<span>{{ convertMin }}</span>:<span>{{ convertSec }}</span><span v-if="timeForState > 0">{{ " / " + timeForState + ":00" }}</span>-->
//             <!--</div>-->
//         <!--</div>-->
//         <!--<div class="bar bar-sm">-->
//             <!--<div class="bar-item" role="progressbar" :style="{width: modePercent + '%'}" aria-valuenow="timeForState" aria-valuemin="0" aria-valuemax="timeForState*60"></div>-->
//         <!--</div>-->
//     </div>
//     `,
//     // computed: {
//     //     convertSec: function() {
//     //         var _actSec = this.second;
//     //         var _minutes = Math.floor(_actSec / 60);
//     //         var _seconds = _actSec - _minutes * 60;
//     //
//     //         if (_seconds < 10) {
//     //             return "0" + _seconds;
//     //         } else {
//     //             return _seconds;
//     //         }
//     //     },
//     //     convertMin: function() {
//     //         var _actSec = this.second;
//     //         var _minutes = Math.floor(_actSec / 60);
//     //         // var _seconds = _actSec - _minutes * 60;
//     //
//     //         if (_minutes < 10) {
//     //             return "0" + _minutes;
//     //         } else {
//     //             return _minutes;
//     //         }
//     //     },
//     //     //wyświetlanie informacji ile jest ustawione czasu dla tryby praca, a aile dla przerwa
//     //     timeForState: function() {
//     //         if (this.state.parent === POMODORO_STATES.WORK.name) {
//     //             return POMODORO_STATES.WORK.time;
//     //         } else if (this.state.parent === POMODORO_STATES.REST.name) {
//     //             return POMODORO_STATES.REST.time;
//     //         } else {
//     //             return 0;
//     //         }
//     //     },
//     //     modePercent: function() {
//     //         return (this.second / (this.timeForState*60)) * 100;
//     //     }
//     // }
// });

// Vue.component('name-and-actions-component', NameAndActionsComponent );
// Vue.component('status-and-info-component', StatusAndInfoComponent );
// Vue.component('counter-component', CounterComponent );

// Vue.component('pomodoro-instance-component', {
//     data: function() {
//         return {
//             appname: "Pomodoro",
//             state: {
//                 name: POMODORO_STATES.START.name,
//                 parent: "", //aktualny status nadrzędny (praca, przerwa)
//                 ico: "play_arrow"
//             },
//             second: 0, //aktualna ilość sekund dla aktywnego trybu (praca, przerwa)
//         }
//     },
//     template: '#pomodoroInstance',
//
// });
//



var Oncecomp ={
    template: `<div class="pomodoroInstance" :id="pomodoroData">
    <div class="pomodoroNameAndActions">
        <span class="pomodoroName" v-bind:class="colour" v-show="!name.changing" @click="changeNameClick()">{{ name.name }}</span>
        <input class="pomodoroNameInput" v-show="name.changing" :value="name.name" @keyup.enter="changeName"/>
        <div class="pomodoroActions">
            <button class="btn" v-bind:class="colour" @click="changeStatus">
                <i class="material-icons">{{ state.ico }}</i>
            </button>
            <button class="btn" v-bind:class="colour" @click="changeMode" :disabled="state.name === 'Get ready?'">
                <i class="material-icons">redo</i>
            </button>
            <button class="btn" v-bind:class="colour" @click="stop" :disabled="state.name === 'Get ready?'">
                <i class="material-icons">stop</i>
            </button>
        </div>
    </div>

    <div class="pomodoroStateAndTools">
        <span class="stateName label" v-bind:class="colour">{{ state.name }}</span>
        <!--<input type="number" class="initTimeToWork form-input input-sm" placeholder="work" title="time to work" v-model="initWorkTime"/>-->
        <!--<input type="number" class="initTimeToRest form-input input-sm" placeholder="rest" title="time to rest" v-model="initRestTime"/>-->
        <div class="pomodoroTools">
            <button class="btn btn-sm" v-bind:class="colour" @click="changeColour"><i class="material-icons">color_lens</i></button>
            <button class="btn btn-sm" v-bind:class="colour" @click="remove"><i class="material-icons">delete</i></button>
        </div>
    </div>

    <div class="toast" v-bind:class="colour">
        <div class="pomodoro-timer">
            <span>{{ convertMin }}</span>:<span>{{ convertSec }}</span><span v-if="timeForState > 0">{{ " / " + timeForState + ":00" }}</span>
        </div>
    </div>
    <div class="bar bar-sm" v-bind:class="colour">
        <div class="bar bar-item" v-bind:class="colour" role="progressbar" :style="{width: modePercent + '%'}" aria-valuenow="timeForState" aria-valuemin="0" aria-valuemax="timeForState*60"></div>
    </div></div>`,

    data: function() {return {
        settingsPomodoro: {
          lastColour: 0
        },
        name: {
            name: "My Pomodoro",
            changing: false
        },
        colour: {
            'colour-my-0': true,
            'colour-my-1': false,
            'colour-my-2': false,
            'colour-my-3': false,
            'colour-my-4': false,
        },
        visible: true,
        state: {
            name: POMODORO_STATES.START.name,
            parent: "", //aktualny status nadrzędny (praca, przerwa)
            ico: "play_arrow"
        },
        times: {
            work: this.initWorkTime,
            rest: this.initRestTime
        },
        second: 0, //aktualna ilość sekund dla aktywnego trybu (praca, przerwa)
        // works: this.statusWork,
        rests: 0
    }},
    // props: ["pomodoroData"],
    props: {
        pomodoroData: String,
        pomodorosCount: Number,
        workCount: Number,
        restCount: Number,
        pauseCount: Number,
        stopCount: Number,
        initWorkTime: {
            type: [Number, String],
            required: true,
            validator: value => value > 0
        },
        initRestTime: {
            type: [Number, String],
            required: true,
            validator: value => value > 0
        }
    },
    watch: {
        initWorkTime: function(newValue) {
            this.times.work = Number(newValue);
        },
        initRestTime: function(newValue) {
            this.times.rest = Number(newValue);
        }
    },
    methods: {
        changeStatus: function() {
            // console.log("aktualny status przed zmianą", this.state.name);
            if (this.state.name === POMODORO_STATES.START.name) {
                this.state.parent = POMODORO_STATES.WORK.name;
                this.start();
            } else if (this.state.name === POMODORO_STATES.WORK.name) {
                this.state.parent = POMODORO_STATES.WORK.name;
                this.pause();
            } else if (this.state.name === POMODORO_STATES.REST.name) {
                this.state.parent = POMODORO_STATES.REST.name;
                this.pause();
            } else if (this.state.name === POMODORO_STATES.PAUSE.name) {
                this.start();
            }
            // console.log("aktualny status po zmianie", this.state.name);
        },

        start: function() { //rozpoczęcie pracy lub wznowienie po zapauzowaniu
            console.log("START");

            //zmiana liczników Pomodoro w header
            if (this.state.name === POMODORO_STATES.START.name) {
                //pierwsze uruchomienie Pomodoro
                this.$emit("update:stopCount",this.stopCount-1);
                this.$emit("update:workCount",this.workCount+1);

            } else if (this.state.parent === POMODORO_STATES.WORK.name) {
                //change pause > work
                this.$emit("update:pauseCount",this.pauseCount-1);
                this.$emit("update:workCount",this.workCount+1);
            } else if (this.state.parent === POMODORO_STATES.REST.name) {
                //change pause > rest
                this.$emit("update:pauseCount",this.pauseCount-1);
                this.$emit("update:restCount",this.restCount+1);
            }

            if (this.state.parent === POMODORO_STATES.WORK.name) {
                this.state.name = POMODORO_STATES.WORK.name;
            } else if (this.state.parent === POMODORO_STATES.REST.name) {
                this.state.name = POMODORO_STATES.REST.name;
            }

            this.state.ico = "pause";
            // this.$emit("update:workCount",this.workCount+1);
            // this.$emit("update:stopCount",this.stopCount-1);
            this.countTime('start');
        },

        stop: function() { //zatrzymanie Pomodoro i zresetowanie do ustawień początkowych
            console.log("STOP");

            //zmiana liczników Pomodoro w header
            if (this.state.name === POMODORO_STATES.WORK.name) {
                //change work > stop
                this.$emit("update:workCount",this.workCount-1);
                this.$emit("update:stopCount",this.stopCount+1);
            } else if (this.state.name === POMODORO_STATES.PAUSE.name) {
                //change pause > stop
                this.$emit("update:pauseCount",this.pauseCount-1);
                this.$emit("update:stopCount",this.stopCount+1);
            }

            this.state.name = POMODORO_STATES.START.name;
            this.state.parent = "";
            this.state.ico = "play_arrow";
            this.countTime('stop');
        },

        pause: function() {
            console.log("PAUSE");

            //zmiana liczników Pomodoro w header
            if (this.state.name === POMODORO_STATES.WORK.name) {
                //change work > pause
                this.$emit("update:workCount",this.workCount-1);
                this.$emit("update:pauseCount",this.pauseCount+1);
            } else if (this.state.name === POMODORO_STATES.REST.name) {
                //change rest > pause
                this.$emit("update:restCount",this.restCount-1);
                this.$emit("update:pauseCount",this.pauseCount+1);
            }

            this.state.name = POMODORO_STATES.PAUSE.name;
            this.state.ico = "play_arrow";
            this.countTime('pause');
        },

        changeMode: function() { //zmiana z praca>przerwa i przerwa>praca
console.log(this.state.parent +">"+ this.state.name)
            //zmiana liczników Pomodoro w header

            if (this.state.name === POMODORO_STATES.PAUSE.name && this.state.parent === POMODORO_STATES.REST.name) {
                //change pause > work
                this.$emit("update:pauseCount",this.pauseCount-1);
                this.$emit("update:workCount",this.workCount+1);
            } else if (this.state.name === POMODORO_STATES.PAUSE.name && this.state.parent === POMODORO_STATES.WORK.name) {
                //change pause > rest
                this.$emit("update:pauseCount",this.pauseCount-1);
                this.$emit("update:restCount",this.restCount+1);
            } else if (this.state.name === POMODORO_STATES.WORK.name) {
                //change work > rest
                this.$emit("update:workCount",this.workCount-1);
                this.$emit("update:restCount",this.restCount+1);
            } else if (this.state.name === POMODORO_STATES.REST.name) {
                //change rest > work
                this.$emit("update:restCount",this.restCount-1);
                this.$emit("update:workCount",this.workCount+1);
            }

            if (this.state.parent === POMODORO_STATES.WORK.name) {
                console.log("Go to REST");

                this.countTime('stop');
                this.state.name = POMODORO_STATES.REST.name;
                this.state.parent = POMODORO_STATES.REST.name;
                this.countTime('start');
                this.state.ico = "pause";
            } else if (this.state.parent === POMODORO_STATES.REST.name) {
                console.log("Go to WORK");

                this.countTime('stop');
                this.state.name = POMODORO_STATES.WORK.name;
                this.state.parent = POMODORO_STATES.WORK.name;
                this.countTime('start');
                this.state.ico = "pause";
            }
        },

        remove: function() {
            //zmiana liczników Pomodoro w header
            if (this.state.name === POMODORO_STATES.START.name) {
                //change start > delete
                this.$emit("update:stopCount",this.stopCount-1);
            } else if (this.state.name === POMODORO_STATES.WORK.name) {
                //change work > delete
                this.$emit("update:workCount",this.workCount-1);
            } else if (this.state.name === POMODORO_STATES.REST.name) {
                //change rest > delete
                this.$emit("update:restCount",this.restCount-1);
            } else if (this.state.name === POMODORO_STATES.PAUSE.name) {
                //change pause > delete
                this.$emit("update:pauseCount",this.pauseCount-1);
            } else if (this.state.name === POMODORO_STATES.STOP.name) {
                //change stop > delete
                this.$emit("update:stopCount",this.stopCount-1);
            }

            this.$emit('delete-pomodoro',this.pomodoroData)
        },

        countTime: function(action) {

            if (action === "start") {
                this._interval = setInterval(function(){
                    this.second += 1;

                    //zmiana trybu na kolejny, kiedy upłynie zadeklarowany czas aktualnego trybu
                    if (this.second === this.timeForState*60) {
                        if (this.state.parent === POMODORO_STATES.WORK.name) {
                            this.workCount =+ 1;
                        } else if (this.state.parent === POMODORO_STATES.REST.name) {
                            this.restCount =+ 1;
                        }
                        this.changeMode();
                    }

                }.bind(this), 1000);
            } else if (action === "pause") {
                clearInterval(this._interval);
            } else if (action === "stop") {
                clearInterval(this._interval);
                this.second = 0;
            }

        },
        changeNameClick: function() {
            this.name.changing = true;
        },
        changeName: function(e) {
            this.name.name = e.target.value;
            this.name.changing = false;
            console.log("Changed name of Pomodoro from " + this.name.name + " to " + e.target.value);
        },
        changeColour: function() {
            // console.log(" aktualny kolor " + this.settingsPomodoro.lastColour);

            if (this.pomodorosCount > 1) {
                //TODO: sprawdzic kolory sąsiednie i nie zmieniać na takie same

                var randomColour = 0;
                do {
                    randomColour = Math.floor(Math.random() * Object.keys(this.colour).length);
                    // console.log(" wylosowano kolor " + randomColour);
                } while (randomColour === this.settingsPomodoro.lastColour);
                // console.log(" Changing colour from " + this.settingsPomodoro.lastColour + " to " + randomColour);
                this.settingsPomodoro.lastColour = randomColour;

                for (var key in this.colour) {
                    if (this.colour.hasOwnProperty(key)) {
                        // console.log(key + " -> " + this.colour[key]);
                        if (key === 'colour-my-' + this.settingsPomodoro.lastColour) {
                            this.colour[key] = true;
                        } else {
                            this.colour[key] = false;
                        }
                    }
                }

            }

        }

    },


    computed: {
        convertSec: function() {
            var _actSec = this.second;
            var _minutes = Math.floor(_actSec / 60);
            var _seconds = _actSec - _minutes * 60;

            if (_seconds < 10) {
                return "0" + _seconds;
            } else {
                return _seconds;
            }
        },
        convertMin: function() {
            var _actSec = this.second;
            var _minutes = Math.floor(_actSec / 60);
            // var _seconds = _actSec - _minutes * 60;

            if (_minutes < 10) {
                return "0" + _minutes;
            } else {
                return _minutes;
            }
        },
        //wyświetlanie informacji ile jest ustawione czasu dla tryby praca, a aile dla przerwa
        timeForState: function() {
            if (this.state.parent === POMODORO_STATES.WORK.name) {
                if (this.times.work === 0) {
                    return POMODORO_STATES.WORK.time;
                } else {
                    return this.times.work;
                }
            } else if (this.state.parent === POMODORO_STATES.REST.name) {
                if (this.times.rest === 0) {
                    return POMODORO_STATES.REST.time;
                } else {
                    return this.times.rest;
                }
            } else {
                return 0;
            }
        },
        modePercent: function() {
            return (this.second / (this.timeForState*60)) * 100;
        }
    },

    created() {
        this.changeColour();
    }
};


var app = new Vue({
    el: "#app",
    data: {
        // quantityComponents: 0,
        pomodoroComponents: [],
        initWorkTime: POMODORO_STATES.WORK.time,
        initRestTime: POMODORO_STATES.REST.time,
        workCount: 0,
        restCount: 0,
        pauseCount: 0,
        stopCount: 0

    },
    watch: {
    },
    methods: {
        addNewPomodoro: function() {
            this.pomodoroComponents.push("P"+this.pomodoroComponents.length + "_" + rand());
            this.stopCount++;
            console.log("Added new Pomodoro");
        },
        pauseAllPomodoros: function() {
            console.warn("TODO: pause all pomodoros");
            //TODO: ustawić liczniki w headerze
        },
        deleteAllPomodoros: function() {
            console.log("Deleted all pomodoros");
            //TODO: dodać modal window z pytaniem czy na pewno usunąć
            this.workCount = 0;
            this.restCount = 0;
            this.pauseCount = 0;
            this.stopCount = 0;
            this.pomodoroComponents = [];
        },
        deletePomodoro: function(value) {
            console.log("Deleted pomodoro " + value);
            removeFromArray(this.pomodoroComponents,value);
        }
    },
    computed: {
        pomodoroCount: function() {
            return this.pomodoroComponents.length;
        }
    },
    components: {"pomodoro-instance-component": Oncecomp},
    created() {
        this.addNewPomodoro();
    }

});


function removeFromArray(array, element) {
    const index = array.indexOf(element);
    array.splice(index, 1);
}

function rand() {
    return Math.floor(Math.random() * 10000) + 1;
}
